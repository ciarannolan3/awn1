from math import ceil

import ieee_standards


# Variables consistent with all 802.11 versions
def control_vars():
    return {
        "snap": 64,
        "tcp_ack": 320,
        ".11_mac": 272,
        ".11_ack": 112,
        "ofdm": 6,
        "rts": 160,
        "cts": 112,
        "packet_size": 1500 * 8
    }


def assemble_protocol_data():
    """Function to retrieve the relevant 802.11 protocol data"""
    # Select IP protocol
    ip = int(input("Please Select:\nUDP = 1\nTCP = 2\n"))

    # ensure valid IP protocol selected
    while ip != 1 and ip != 2:
        ip = input("Please Select 1 [UDP] or 2 [TCP]")

    # select mac_protocol
    mac_protocol = int(
        input("Please select a Medium Access Control Protocol from the following list, using the provided number:\n"
              "[0] 802.11a\n"
              "[1] 802.11g\n"
              "[2] 802.11n\n"
              "[3] 802.11ac Wave 1\n"
              "[4] 802.11ac Wave 2\n"
              "[5] 802.11ax\n"))

    # ensure valid mac protocol selected
    while (not 0 <= mac_protocol <= 5) or isinstance(mac_protocol, str):
        mac_protocol = int(input("Invalid input, please try again"))

    # round any data rates to one decimal place
    avail_rates = ieee_standards.avail_rates(mac_protocol)
    print("The available data rates are specified below. Use the integer marker to select.")

    # print available rates
    for i in range(len(avail_rates)):
        print("[" + str(i) + "] ", avail_rates[i])

    data_rate = int(input("\nPlease specify a data rate: "))

    # ensure valid selection made
    while (not 0 <= data_rate <= len(avail_rates)) or isinstance(data_rate, str):
        data_rate = int(input("Invalid data rate, please try again: "))

    # dictionary of lambda functions to retrieve relevant 802.11 protocol data
    protocol_data_retrieval = {
        0: lambda: ieee_standards.a_g_data(avail_rates[data_rate], 0),
        1: lambda: ieee_standards.a_g_data(avail_rates[data_rate], 1),
        2: lambda: ieee_standards.n_data(avail_rates[data_rate]),
        3: lambda: ieee_standards.ac_data(avail_rates[data_rate], 3),
        4: lambda: ieee_standards.ac_data(avail_rates[data_rate], 4),
        5: lambda: ieee_standards.ax_data(avail_rates[data_rate])
    }

    # retrieve data using lambda function
    function = protocol_data_retrieval.get(mac_protocol, lambda: 'Invalid')
    data_rate_info, protocol_timing_info = function()

    print("Chosen Internet Protocol: ", ip, "\n Chosen MAC Protocol: ", mac_protocol, "\n Selected Data Rate: ",
          avail_rates[data_rate])
    return data_rate_info, protocol_timing_info, ip, mac_protocol


def calculate_frame_interchange_time(protocol_timing_info, ip, nss):
    """Function to calculate Preamble and frame interchange times for TCP/UDP transmissions"""
    # Single Data Exchange: 1 x DIFS, 3 x SIFS, 4 x Preamble

    # Selecting the correct preamble size
    if nss > 1:
        preamble = "preamble_max"
    else:
        preamble = "preamble_min"

    if ip == 1:
        return protocol_timing_info["difs"] + 3 * protocol_timing_info["sifs"] + 4 * protocol_timing_info[preamble]
    else:
        # two exchanges to account for TCP
        return (protocol_timing_info["difs"] + 3 * protocol_timing_info["sifs"] + 4 * protocol_timing_info[
            preamble]) * 2


def calculate_control_frame_times(protocol_timing_info, bits_per_symbol, ip):
    """
    Each data transmission consists of a number of Control Frames:
    1x RTS, 1xCTS, 1xACK:

    In the case of UDP, there is a single transmission flow. With TCP, to account for the TCP ACK, a second
    transmission takes place, meaning there are two sets of RTS, CTS, ACK

    OFDM tail: 6 bits
    RTS: 20 bytes = 160 bits + tail = 166 bits
    CTS: 14 bytes = 112 bits + tail = 118 bits
    ACK: 14 bytes
    """

    # Handle RTS,CTS,ACK
    control = control_vars()
    rts = control["rts"] + control["ofdm"]
    cts = control["cts"] + control["ofdm"]
    ack = control[".11_ack"] + control["ofdm"]

    # Calculate the symbol sizes for the control frames
    rts_symbol_size = ceil(rts / bits_per_symbol)
    cts_symbol_size = ceil(cts / bits_per_symbol)
    ack_symbol_size = ceil(ack / bits_per_symbol)

    # Account for .11g Signal extension
    if "sigextension" in protocol_timing_info:
        sig_extension = protocol_timing_info["sigextension"]
    else:
        sig_extension = 0

    # Calculate the total time the control frames contribute to the overall transmission time
    total_time = rts_symbol_size * protocol_timing_info.get("sdur") + cts_symbol_size * protocol_timing_info.get(
        "sdur") + ack_symbol_size * protocol_timing_info.get("sdur") + 3 * sig_extension

    # if ip is TCP (2), need two exchanges calculated, otherwise UDP (1)
    return total_time * ip, (ack + rts + cts) * ip


def calculate_data_frame_time(bits_per_symbol, protocol_timing_info, ip):
    """
        In the case of UDP, there is a single transmission flow. With TCP, to account for the TCP ACK.

        SNAP header = 8 bytes = 64 bits, OFDM tail = 6bits
    """

    # Account for .11g Signal extension
    if "sigextension" in protocol_timing_info:
        sig_extension = protocol_timing_info["sigextension"]
    else:
        sig_extension = 0

    # retrieve the variables consistent across all protocols
    control = control_vars()
    # calculate the data frame size
    data_frame_size = control["snap"] + protocol_timing_info["mac"] + control["packet_size"] + control["ofdm"]

    # calculate symbol size
    data_symbol_size = ceil(data_frame_size / bits_per_symbol)

    # if TCP calculate ACK
    if ip == 2:
        ack_frame_size = control["snap"] + protocol_timing_info["mac"] + control["tcp_ack"] + control["ofdm"]
        ack_symbol_size = ceil(ack_frame_size / bits_per_symbol)
        return ((data_symbol_size + ack_symbol_size) * protocol_timing_info[
            "sdur"]) + 2 * sig_extension, data_frame_size, ack_frame_size
    else:
        return data_symbol_size * protocol_timing_info["sdur"] + sig_extension, data_frame_size, 0


def main():
    # retrieve data info and control variables
    data_rate_info, protocol_timing_info, ip, mac = assemble_protocol_data()
    control = control_vars()

    # compute normal case: (1 NSS), 20MHz Channel or .11a/g
    bits_per_symbol_normal = int(
        data_rate_info["Nbits"] * data_rate_info["CRate"] * protocol_timing_info["normal_nchan"])
    # control frame time and bits
    control_frame_time_normal, control_frame_bits_normal = calculate_control_frame_times(protocol_timing_info,
                                                                                         bits_per_symbol_normal, ip)
    frame_interchange_time_normal = calculate_frame_interchange_time(protocol_timing_info, ip, 1)
    data_frame_time_normal, data_frame_bits_normal, ack_frame_bits_normal = calculate_data_frame_time(bits_per_symbol_normal,
                                                                                                      protocol_timing_info,
                                                                                                      ip)

    total_transmission_time = control_frame_time_normal + frame_interchange_time_normal + data_frame_time_normal
    throughput = (control["packet_size"]) / total_transmission_time

    # find the time to transfer 10GB of data
    ten_gb_time_normal = (10e3 * 8) / throughput
    print("\n**Normal Case**\nBits Per Symbol:", bits_per_symbol_normal, "" + "\nData throughput in Mbps:",
          round(throughput, 3), "\nTen GB Transfer time in Seconds: ", round(ten_gb_time_normal, 2))

    # Compute "Best Case"
    if mac > 1:
        bits_per_symbol_max = int(data_rate_info["Nbits"] * data_rate_info["CRate"] * protocol_timing_info["max_nchan"]
                                  * protocol_timing_info["max_nss"])

        control_frame_time_max, control_frame_bits_max = calculate_control_frame_times(protocol_timing_info,
                                                                                       bits_per_symbol_max, ip)
        frame_interchange_time_max = calculate_frame_interchange_time(protocol_timing_info, ip,
                                                                      protocol_timing_info["max_nss"])
        data_frame_time_max, data_frame_bits_max, ack_frame_bits_max = calculate_data_frame_time(bits_per_symbol_max, protocol_timing_info, ip)

        total_transmission_time = control_frame_time_max + frame_interchange_time_max + data_frame_time_max
        throughput_max = (control["packet_size"]) / total_transmission_time
        ten_gb_time_max = (10e3 * 8) / throughput_max

        print("\n**Best Case -", protocol_timing_info["max_nss"], "Spatial Streams,",
              protocol_timing_info["channel_max"], "MHz Channel**\nBits Per Symbol:", bits_per_symbol_max,
              "\nData throughput in Mbps:", round(throughput_max, 3), "\nTen GB Transfer time in seconds: ",
              round(ten_gb_time_max, 2))
        return ten_gb_time_normal, ten_gb_time_normal, throughput_max, ten_gb_time_max

    else:
        return ten_gb_time_normal, ten_gb_time_normal


if __name__ == '__main__':
    main()
