# Advances in Wireless Networking COMP40660, Assignment 1
## Name: Ciaran Nolan
## Student Number: 15329936

### Task:
Design and implement a throughput calculator for a chosen number of 802.11 wireless standards, as specified in the assignment descriptor.
The relevant info for each protocol was provided to implement a "normal" and "best" case design, where a 20MHz, 1 
Spatial Stream implementation is considered normal, and the widest channel and largest number of spatial streams 
(varying from protocol to protocol) was considered best. This calculator was required to supports both UDP and TCP 
protocols.

#### Deliverables:
Throughput for normal and best case configurations, for any of the required protocols at any data rate, in either 
TCP or UDP.

The time taken for transfer of 10GB using this protocol configuration and its above determined throughput .

### Submission:
This submission contains this README, along with two python files; "main.py" and "ieee_standards.py". This project uses 
an import from the native math library and no other imports, besides locally created files.

"main.py": responsible for computing all relevant calculations for throughout/10GB transfer time as well as user I/O. 
The file splits the calculations into a number of functions to increase human readability

"ieee_standards.py": contains all data relating to 802.11 standards required for this assignemt as provided in the 
assignment descriptor.  

### Usage:
This project requires python3.x due to the use of integer division.
Run the main.py file using an IDE or by executing "python main.py" or "python3 main.py", depening on your install of 
python, into a terminal.

Upon execution, the user is required to make a number of decisions, using the provided integer menus:

- UDP or TCP (1 / 2)
- Choice of 802.11 Protocol
- Data rate corresponding to "normal case"

Output:

- Data Throughput, in Mbps
- Time taken, in seconds to transfer 10GB of data

![image](sample_calculation.PNG)
### Why there is a difference between the actual throughput and the advertised data rate
 The main reason for the discrepancy in the advertised data rate and observed through out is a result of the 802.11 
 protocols use of overhead and inter frame spacing. Frame interchange spacing and signal extensions (802.11g) have no 
 actual transfer of data taking place, extending the required time to transmit a packet of data, as they are required 
 for both control and data frames
 
 Similarly, in order to transmit TCP/UDP data, MAC control frames are required, as per the 802.11 protocol. With RTS, 
 CTS and 802.11 ACK frames accounting for 20, 14 and 14 bytes respectively, this adds a sizeable increase with overhead
 of frames. Each frame, which includes RTS, CTS, data and 802.11 ACK frames can receive overhead of more than 40 bytes.
 
 Due to the nature of TCP as a protocol, each packet of data requires an acknowledgement of reception. Therefore, a 
 full data exchange has twice the number of control frames and interspace times, which reduces throughput noticeably in 
 comparison to UDP. With TCP ACK being approximately 40 bytes, the data to overhead ratio is nearly 1:1. Hence we can 
 make an observation that the actual throughput will always be less than the advertised data rate, due to the 
 aforementioned reasons
 
 ### 802.11 performance improves after each release. Briefly discuss the trade-offs involved in such improvements.
 
 With newer 802.11 protocols comes better performance. This is often achieved through the introduction of higher 
 capacity data channels increasing the protocols bandwidth. For example, from 802.11ac wave 1 to wave 2, very high 
 throughput (VHT) channels were introduced, allowing for channels up to 160MHZ. While this theoretically improved performance,
 due to the protocol having quadruple the space to transmit data, and could therefore transmit four times the amount of
 data it has a number of draw backs. More populated channels are more susceptible to noise, as well as these larger channels 
 having a reduced effective range, due to a decrease in power density. Transmitting further distances requires much 
 larger power contributions. The same case for reduced range can be seen as protocols begin to support both 2.4GHz and 
 5GHz frequency bands.
 
 The MIMO Streams, which were first introduced in 802.11n, resulted in the ability to increase the number of spatial 
 streams from 4 to 8. Increasing the number of streams has a proportional relationship to the capacity for data 
 transmission. The main draw back is the lack of commercial producers providing some equipment capable of utilizing such 
 advancements. MU-MIMO, introduced in 802.11ac, augments the current MIMO implementation, in cases where the number of 
 connections on the MIMO system is greater than one.   