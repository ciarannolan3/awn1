def avail_rates(mac_protocol):
    # function to display all available rates to user
    avail_rates = [
        [6.0, 9.0, 12.0, 18.0, 24.0, 36.0, 48.0, 54.0],
        [6.0, 9.0, 12.0, 18.0, 24.0, 36.0, 48.0, 54.0],
        [7.2, 14.4, 21.7, 28.9, 43.3, 57.8, 65.0, 72.2],
        [7.2, 14.4, 21.7, 28.9, 43.3, 57.8, 65.0, 72.2, 86.7, 96.3],
        [7.2, 14.4, 21.7, 28.9, 43.3, 57.8, 65.0, 72.2, 86.7, 96.3],
        [8.6, 17.2, 25.8, 34.4, 51.6, 68.8, 77.4, 86.0, 103.2, 114.7, 129.0, 143.4]
    ]
    return avail_rates[mac_protocol]

# all relevant 802.11 protocols with relevant info stored as dictionaries

def a_g_data(data_rate, mac):
    # Protocol 0/1

    data_rates = {
        6.0: {"Nbits": 1, "CRate": 0.5},
        9.0: {"Nbits": 1, "CRate": 0.75},
        12.0: {"Nbits": 2, "CRate": 0.5},
        18.0: {"Nbits": 2, "CRate": 0.75},
        24.0: {"Nbits": 4, "CRate": 0.5},
        36.0: {"Nbits": 4, "CRate": 0.75},
        48.0: {"Nbits": 6, "CRate": 2 / 3},
        54.0: {"Nbits": 6, "CRate": 0.75},
        "a_timings": {"difs": 34, "sifs": 16, "sdur": 4, "normal_nchan": 48, "preamble_min": 20, "odfm": 6, "mac": 272},
        "g_timings": {"difs": 28, "sifs": 10, "sdur": 4, "normal_nchan": 48, "preamble_min": 20, "odfm": 6, "mac": 272, "sigextension": 6}
    }

    if mac == 0:
        return data_rates[data_rate], data_rates["a_timings"]
    else:
        return data_rates[data_rate], data_rates["g_timings"]


def n_data(data_rate):

    data_rates = {
        7.2: {"Nbits": 1, "CRate": 0.5},
        14.4: {"Nbits": 2, "CRate": 0.5},
        21.7: {"Nbits": 2, "CRate": 0.75},
        28.9: {"Nbits": 4, "CRate": 0.5},
        43.3: {"Nbits": 4, "CRate": 0.75},
        57.8: {"Nbits": 6, "CRate": 2 / 3},
        65.0: {"Nbits": 6, "CRate": 0.75},
        72.2: {"Nbits": 6, "CRate": 5 / 6},
    }
    n_timings = {"difs": 34, "sifs": 16, "timeslot": 9, "sdur": 3.6, "preamble_min": 20, "preamble_max": 46,
                  "odfm": 6, "mac": 320, "max_nss": 4, "normal_nchan": 52, "max_nchan": 108, "channel_max": 40}

    return data_rates[data_rate], n_timings


def ac_data(data_rate, mac):
    # handle channel limitation of ac wave 1

    data_rates = {

        7.2: {"Nbits": 1, "CRate": 0.5, "NChan": 52},
        14.4: {"Nbits": 2, "CRate": 0.5, "NChan": 52},
        21.7: {"Nbits": 2, "CRate": 0.75, "NChan": 52},
        28.9: {"Nbits": 4, "CRate": 0.5, "NChan": 52},
        43.3: {"Nbits": 4, "CRate": 0.75, "NChan": 52},
        57.8: {"Nbits": 6, "CRate": 2 / 3, "NChan": 52},
        65: {"Nbits": 6, "CRate": 0.75, "NChan": 52},
        72.2: {"Nbits": 6, "CRate": 5 / 6, "NChan": 52},
        86.7: {"Nbits": 8, "CRate": 0.75, "NChan": 52},
        96.3: {"Nbits": 8, "CRate": 5 / 6, "NChan": 52},

        "ac_w1_timings": {"difs": 34, "sifs": 16, "timeslot": 9, "sdur": 3.6, "preamble_min": 20, "preamble_max": 56.8,
                          "odfm": 6, "mac": 320, "max_nss": 3, "normal_nchan": 52, "max_nchan": 234, "channel_max": 80},
        "ac_w2_timings": {"difs": 34, "sifs": 16, "timeslot": 9, "sdur": 3.6, "preamble_min": 20, "preamble_max": 92.8,
                          "odfm": 6, "mac": 320, "max_nss": 8, "normal_nchan": 52, "max_nchan": 468, "channel_max": 160}
    }
    if mac == 3:
        return data_rates[data_rate], data_rates["ac_w1_timings"]
    else:
        return data_rates[data_rate], data_rates["ac_w2_timings"]


def ax_data(data_rate):

    data_rates = {

            8.6: {"Nbits": 1, "CRate": 0.5, "NChan": 234},
            17.2: {"Nbits": 2, "CRate": 0.5, "NChan": 234},
            25.8: {"Nbits": 2, "CRate": 0.75, "NChan": 234},
            34.4: {"Nbits": 4, "CRate": 0.5, "NChan": 234},
            51.6: {"Nbits": 5, "CRate": 0.75, "NChan": 234},
            68.8: {"Nbits": 6, "CRate": 2 / 3, "NChan": 234},
            77.4: {"Nbits": 6, "CRate": 0.75, "NChan": 234},
            86.0: {"Nbits": 6, "CRate": 5 / 6, "NChan": 234},
            103.2: {"Nbits": 8, "CRate": 0.75, "NChan": 234},
            114.7: {"Nbits": 8, "CRate": 5/6, "NChan": 234},
            129.0: {"Nbits": 10, "CRate": 3/4, "NChan": 234},
            143.4: {"Nbits": 10, "CRate": 5/6, "NChan": 234},

        "ax_timings": {"difs": 34, "sifs": 16, "timeslot": 9, "sdur": 13.6, "preamble_min": 20, "preamble_max": 92.8,
                       "odfm": 6, "mac": 320, "max_nss": 8, "normal_nchan": 234, "max_nchan": 1960, "channel_max": 160}
    }
    return data_rates[data_rate], data_rates["ax_timings"]
